
*This document is a work in progress and might be missing a lot of information.*

Auro intends to be a sort of Lisp for machines, easily understood and with fundamental constructs simple enough that it can be easily manipulated programatically in itself, hopefully allowing unrestricted abstraction power. It also tries to have enough information to make compilation and interpretation relatively easy.

Another goal is to fit whole programs in an easily parsable and compact format.

## Items

Items are the items that Auro loaders know and manipulate. There are three kinds of items

- Modules, the main source for most items
- Types, determine what values are allowed anywhere a value can be
- Functions, receive and return values at runtime and can modify the state of the context

Functions describe the runtime behaviour of programs. Types are used to aid compiling and static analysis. Modules are necessary as items so that they can be processed and passed around and reasoned about the same way as functions and types.

## Registers

The registers are a fixed size collection of slots storing values. Code functions creates an independent register for every value coming out of a function, and then allows these registers to be mutated. This is more or less a simple register model with some SSA characteristics.

This was chosen because:
- easy to generate by language compilers
- simple to transform into a more compact register allocation
- shifts the burden of efficient register allocation to the platform
- slightly simpler to transform to SSA than normal register based code
- resulted in smaller sizes (not rigurously tested or demonstrated afaik, least of all by me)