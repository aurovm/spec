# Auro 0.6

*[2018-03-23 05:23]*

The file starts with the ascii string `Auro 0.6` followed by NUL.

Every integer is encoded in varint encoding: each byte has the least significant 7 bits be part of the integer bits, and the most significant indicates if there's another byte in the number. A number stops at the first byte with an unsigned value less than 128.

A module file has 5 sections: Modules, Types, Functions, Code and Metadata. The first three are item sections, entries in the module and function sections must have a kind id, which indicates how the item is defined, and in this document each item kind will have it's id indicated in square brackets in it's title.

Each section has for the first byte an integer indicating how many items are present in that section, except for the code section, whose count is implicit.

**TODO:** Document module level manipulation, as implemented in Aurovm.

# Modules

A module is a structure containing items indexed by name. An item may be a type, a function or another module, and each one of those sections can use items from a module.

A functor is one whose its items cannot be accessed until another module is passed as a parameter to it (the name is borrowed from ML). A functor's construction may fail if the module expects items not present in the argument or those items don't hold assumtions made by the functor.

A module file has two special module indices: the module at index 0 is the module passed as a parameter to the file module, and the module at index 1 is the module that will be exported by the file. The modules declared in the module section start at index 1.

The module defined by a module file is put in the global module namespace by the implementation, and it's up to the implementation to decide what name to use, it may use file system information or metadata. I intend to define a standard mechanism to do determine the name of a module file in the global module namespace.

Each module defines it's argument equivalence with other modules, if to the module judgement it's argument is equivalent to the argument of a previously built module, that module is returned instead. Modules defined by a file consider their arguments equivalent if the items only type items are used and they are equal, or if they don't use items from their arguments. Using items from a module defined by a file without passing it an argument is the same as building that module with an empty argument module.

By convention, modules that don't use names in their arguments items expect those items to be named with their position in decimal notation starting from 0.

By convention, modules that have one main item export it with an empty string as the name, such as Java classes, Python modules or Java generic methods.

## Unknown [0]

## Import [1]

    name: string

Gets a module from the global module namespace constructed with an empty module argument.

## Define [2]

    len: int
    items: {
        kind: int
        index: int
        name: string
    }[len]

Creates a module with the specified items. Each item is defined by a section (a number indicating which of the module[0], type[1] or function[2] section is the item from), an index to an item of said section, and a name with which the item will be accessed from the module.

## Use [3]

    module: module_index
    name: string

Gets a functor imported from another module

## Build [4]

    functor: module_index
    argument: module_index

Applies an argument to a functor

# Types

    module: module_index
    name: string

Types do not have kinds, all types are always imported from a module, and the standard library provides certain special type creating modules. The type is imported from the module with index `module - 1`. If the module index is 0, the module where the type is from is hidden, useful for documentation modules.

# Functions

    kind: int
    in_count: int
    in_types: type_index[in_count]
    out_count: int
    out_types: type_index[out_count]

## Unknown [0]

A function that is guaranteed to exist but isn't defined in the module file, for example in documentation modules.

## Code [1]

A function whose code is defined in the code section.

## Import

    name: string

If the kind is > 1, the function is extracted from the module with index `kind - 2`, and has an aditional string for the name after the usual ones.

# Constants

Defines functions that returns single constant values, and are added to the function list. In spite of these just being functions, they need to be separated from the main function section because to parse this section, the inputs and outputs of all functions need to be known.

## Int [1]

    value: int

A value of type cobre.int (only positive numbers).

## Buffer [2]

    size: int
    data: byte[size]

A value of type cobre.buffer.

## Call

Any item with kind > 15 is a function call, formated exactly like a regular function calls in code, but the arguments are indices to other functions (of exactly one result, if a function with more is to be used, a constant for each result must be created beforehand). Each value the function returns is added independently as a constant. Constants are aware of each other, even if defined after, but circular calls are not allowed.

# Code

Each one of the blocks is linked to the code function declared with the same index, relative to the other code functions. The number of code items is implicit, counting the function items of kind Code.

The number of instructions in a Code item is specified in the beggining.

## Bytecode

If an instruction index is less than 16 it's a basic instruction, otherwise is an index to an function. Each value that results of an instruction is assigned an index incrementally, the index of an instruction is not the same as the index of its values as some have many results and some none. If a value is used only once after it's created it's a temporary, if it's used more times or is reassigned later it becomes a variable, they should be treated differently by an optimal implementation because using a different memory location for each temporary is too inefficient.

The basic instructions are:

- `0 end`: Returns, taking all the result values as arguments.
- `1 hlt`: Halts the execution of the machine, "panics".
- `2 var`: Creates a null variable that must be assigned before used.
- `3 dup (a)`: Copies the value at *a* (*var+set?*).
- `4 set (b a)`: Assigns to the variable at *b* the value at *a*.
- `5 jmp (i)`: Jumps to the instruction with index *i*.
- `6 jif (i a)`: If the value at *a* is true, jumps to *i*.
- `7 nif (i a)`: If the value at *a* is false, jumps to *i*.

All execution paths of a function must end with an end instruction, even if there's no return type.

# Metadata

The metadata is structured like s-expressions with a custom binary encoding. Each element starts with a varint, the first bit indicates if the item is a list (0) or a string (1), and the rest of the bits indicate the number of subelements for lists or the number of characters for strings. The list bit is 0 so that the byte 0 represents an empty list, the closest value to nil.

# Functor Module Files

Module files can define functors in two ways.

If there's a meta node with a *"type arguments"* header, the tail of the node must be all strings and are the names of the argument module members which are used as type arguments, and module equality depends on the equality of those types.

If there's a meta node with a *"functor module"* header, it must have no tail and it means that each instantiation of the module is different from every other.

If none of these is present, the default behaviour is that the module is not a functor.

To be honest, it seems not really that nice to me, it's probable that I will change the mechanism for defining functor modules.
