# Complex Modules

Because these modules are complex, they can't be described by an auro source file, so they are textually described in this document. Many of these modules allow the manipulation of top level items from regular code.

## Type

The module `auro.type` defines runtimpe type, contains:

- a type ` ` for type value
- a functor module `new`, accepts a type  ` ` and results in a function ` ` that returns at runtime the value that represents the given type

## Typeshell

The module `auro.typeshell` is a functor module that accepts a type `0` and results in

- A type ` `. This type will always be different in across instances of the functor, even if they share the type argument
- A function `new` that takes a value of the original type and returns a value of the new type that "contains" the passed value
- A function `get` that takes a value of the new type and returns the value it "contains"

## Struct

The module `auro.struct` is a functor that receives types named with the non zero integers in base 10, every type given sequentially starting from 0 is considered an argument type.

- A type ` ` that represents a value that contains values of each of the argument types
- A function `new` that has the same number of parameters as argument types, each being it's corresponding type, and returns a struct value
- The functions `getN` where N is the value index, which accepts a struct value and returns the containng value at index N
- The functions `setN` where N is the value index, which accepts a struct value and a value of the type with index N, and stores it in the struct value

## Any

The module `auro.any` is a functor, when used directly or without item arguments, the module contains a type ` ` which represents a value of any type, and it's actual non-any type can be requested at runtime.

When used with a type argument `0`, the resulting module contains

- A function `new` that accepts a value of the type argument and returns an any value
- A function `get` that accepts an any value and returns a value of the type argument only if the any value was of that type originally. Calling this function on an any of another type is an error
- A function `test` that accepts an any value and returns a boolean indicating wether the any value was originally of the argument type

## Function

The module `auro.function` is a functor that accepts a module with types `in0`, `in1` etc. and `out0`, `out1`, etc., which define the signature of a function type. The resulting module has:

- a type ` ` that represents runtime functions conforming to the specified signature
- a function `apply` with the same signature of the function but aditionally accepts as first argument the function value
- a functor module `new`, which accepts a function `0` and results in a module with a function ` ` that returns the given function as a value
- a functor module `closure` which accepts a **base function** `0` that has the same signature as the function module, but with an extra last **bound parameter**, and results in a module with
    - a function `new`, that accepts a single **bound value**, and returns a function with the signature of the function module, but when applied, the base function is called and it's last argument is always the bound value

## Module

The module `auro.module` contains:

- a type ` ` which represents a module value
- a function `get` which takes a module value and a string and gets the requested item from the module as a value of type item
- a function `build` that builds a base functor module passing as argument another module
- a functor module `new` that results in a module with a function ` `, which returns the whole argument module as a runtime value of type module
- a functor module `create` that accepts three items that define the behaviour of the resulting module
    + a type `ctx` which indicates the type of the module context value
    + a function `build` which accepts a module value and returns a context value
    + a function `get` which takes a context value and a string name and returns an item

## Items

The module `auro.module.item` contains:

- a type ` ` which represents a polymorphic top level item
- a function `null` that returns an item value that represents no item
- the functions `type`, `code` and `module` that each take a value of the named type and returns an item value
- a function `isnull` that checks if the value is null
- a functor module `function` that accepts a module of the same form as the argument of the `auro.function` functor, and returns a function ` ` that takes a function value with the specified signature and returns an item value
