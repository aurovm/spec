
*This document is a work in progress and might be missing a lot of information.*

Auro has two contexts of computation with different semantics: Load time and Run time. But in practice those are mixed: modules like `create` in `auro.module` takes functions that will be executed at load time, for when all runtime semantics apply; conversely, if provided by the platform, some reflection module could allow loading modules at runtime.

## Data Model

**Items** are the units of information in the Auro loader. There's modules, types and functions. They can be stored in 

**Values** are the minimum unit in Auro, and it has no visible information except it's type. Runtime information about it can only be extracted from functions. At runtime, values can be copied and passed between functions, all functions act like black boxes.

**Types** also have very litle information associated with them, except for whether they are the same.

Usually, only the module that defined a type has access to more information about it. Some types are defined in platform modules, but for all other types, following it's definition will allways lead back to the platform types. Similarly, all values must come from platform defined functions.

In some platforms, the basic types might be basic data types and system types, but other platforms might only offer Javascript Values, translating all other values to them.

A module exposing a type doesn't necessarily mean that it created it, or that it's the only one with access to it's internals. If the definition of the type is known and used by an user of a module, it can pass values of that type coming from the module, and use them in other functions that take the same type.

If a module has functions that uses values of types created by it but not exposed, then those functions are unsusable. All users must know the type of all the values they use (why?).

Inside code functions, the **registers** are slots where values reside, and they are passed to and from functions. All functions take values and return others. At the beggining of execution of a function, the first registers contain it's parameters.

Every function call has a register implicitly associated with all of it's returned values, and some instructions can create registers as well.


## Loading

Loading is the process of preparing an item for being used during execution. Strictly speaking, only the items actually used need loading, but Auro doesn't make that guarantee and some platforms may load all items in a module file, or even all modules in the system. For this, tools are encouraged to warn about unloadable items.

All global modules (from `import` modules) are loaded only once, when that happens is up to the platform (maybe when first used at runtime, or at system startup), and the order in any subset of items is also not guaranteed except in specific cases.

The platform must first choose a function for execution, before which that function must be loaded. It does not necessarily need to load any other item before executing itself, but any internal function called inside it must be loaded before it's called.

By convention, the `main` member of a module is usually its starting point.

For module files, all items defined in them must be loaded at most once (per module instance), except for members of context modules or any other item that uses them, those will be loaded at most once per instance.

Functors (parameterized modules) are only loaded once built (instantiated), and every build loads separately.


## Shared data

Code running at load time is in a separate execution context from runtime code, and from other loads either at runtime or in nested loads, care must be taken when working with shared data, and explicitly use primitives that can guarantee some aspects about the data or execution model.

The main possible mechanism to share data across contexts is a constant item, which is a function that always returns a constant, which is computed at load time.

Depending on how the platform manages contexts and constants, that could mean that load time and runtime can share data, that load times between each other can share data, or neither. Platforms could even enforce immutability in constants, disallowing sharing of data completely.

Although Auro doesn't forbid any of these scenarios, **safe** Auro implementations should *at least* warn the user when, either the platform is unable to live to some expected behaviour, or when the expected behaviour is ambiguous.

Platforms can help with this by offering modules that offer shared data with clear semantics. Ideally, a standard set of library definitions would be maintained with standard semantics that plaforms could adhere to.


## Runtime Execution

When the platform decides to execute a function, a runtime context starts for it. The code in the function is imperative and sequential. Every function calling instruction starts the execution of that function with the given parameters, wait for the function to end, and goes to the next instruction. Other instructions jump to different instructions or move and copy values between registers.

Some functions modify values, and since other functions can have copies of those values, they would see the changes.