# Auro

Auro is an abstract machine specification, simple enough to make it easy to write a complete implementation from scratch, and versatile enough to represent a wide range of languages and paradigms.

Ideally, Auro would be a platform in which a developer can write in any language and can interact easily with modules written in any other, which can run in a lot of different platforms, like different OS's, the web, the JVM or embedded in games and applications.

It's intended that many features available in many programming languages or elsewhere, like OOP, dynamic dispatch, object serializing, etc. be implemented in standard libraries and conventions, instead of in the Auro spec itself.

## Implementations

- [Nugget](https://gitlab.com/aurovm/nugget)
- [Javascript Compiler](https://gitlab.com/aurovm/auroweb#auro-web)
  + [Online Editor](http://arnaud.com.ve/auro/)

## Languages

- [Lua](https://gitlab.com/aurovm/aulua#lua-auro-compiler)
- [Aulang](https://gitlab.com/aurovm/aulang#aulang)

# Spec

This project hosts the specification and documentation of the Auro abstract machine. [Module Format](Module Format.md) describes the Auro module format, and [lib](lib/) contains Auro's standard libraries in culang description files. Some modules are too complicated to be described by a source file, they are explained in [Complex Modules](Complex Modules.md).

More detailed information about the [data and execution models](model.md).

# Motivations

I like the web, but I don't like Javascript. I like Python's libraries, but I don't like Python. I love Scala, but the JVM is too heavy. I like scripting games, but there's more than Lua. I want functional algorithms, but GHC is too big.

I want to develop in any platform, in any language I want, with the libraries I like.

*I also want all of my programs to be **as fast as Cee**. One can only dream, right?*

There's more on the [motivation for design decisions](design.md).

# Similar projects

- __JVM__: The most popular and the main example of a virtual machine. It has a big ecosystem of libraries, frameworks and amazing languages like Scala and Clojure. The problem is that it's very big and complex and making a compilant implementation is very very difficult, another is that it's built specifically around the Java language, and any alternative language implementation must adapt to the Java way to interoperate with the ecosystem.
- __CLI/.Net__: Microsoft's Java, and as such just as inadequate. The first difference is that the main imlementation (and because of the complexity almost the only one) is Windows specific, which strongly conflicts with Auro's multiplatforms objective. The other difference is that it's designed to be more language neutral, and that's good but it's not enough to compensate the downsides.
- __Parrot__: It's a virtual machine and language spec portable across languages, but similar to the JVM, it has a bias towards Perl in it's design and feature set and it makes it very hard for static analysis or compilation.
- __LLVM__: A compiler intermediate representation, not by itself a compilation target. LLVM IR files are not distributable, so one has to distribute either the source code and expect users to have the language's compiler installed, or the machine code and only support users with a specific system setup, or distribute diferent binaries for each system flavour (potentially **a lot**).
- __Lua__, __Scheme__: These are not in the same scope as other projects, but they are included as examples of how a very simple and straightforward design welcomes involvement and progress. Scheme for example has a lot of fully compilant implementations, and Lua has one of the (if not *the*) fastest scripting languages implementations, written by a single person.
- __Javascript__: Javascript is also very different from these projects in design, but it's currently the most popular software distribution format for many languages and projects, by necessity, which hurts the web ecosystem because it wasn't designed for that. Trying to implement a compilant Javascript engine is very complicated, contributing to make the web a de facto monopoly.

**TLDR**: Most of the existing projects are too big and complex for one person to understand sufficiently, and those that aren't (like Lua and WebAssembly), are not versatile enough.

## Webassembly

Webassembly is the project most similar to Auro in overall purpose, and it has a lot of traction, with support from many major internet players. Their system is based of real cpu designs and instruction sets, making it very cpu like.

I believe a simpler, more abstract design offers different advantages, like making it easier to maintain the health of the project, tools and ecosystem, and allowing a wider variety of compilation targets, possibly more abstract than a straight up machine. 

Compiling for real machines is hard, that's why scripting languages are far more popular. There's a lot of manpower spent in reimplementing the same things over and over again. Offering an abstract compilation target saves work for language implementors and inter-language communication.

It would also make it easier to make rough implementations in experimental targets, as the whole system is much smaller and faster to code.

Webassembly is actually an ideal compilation target for Auro, as well as Javascript. Auro is operating at a higher level of abstraction, and it's focusing on a different set of targets than webassembly (including webassembly).

# Tasks to do

- Strong type system in Auro and Aulang
  - Aulang Generics and extensions
  - Auro interface/trait module
- A Javascript frontend demo
- Update the web compiler
  - Functor module definitions
  - Module computation
- Update Lua
- Package Manager
- Update the C based compiler

# Beyond

- Compiled *web-stack* based apps. Eventually even the web apis could be supported in Auro.
- Access to easier languages, like Lua, for web development. Useful for learners.
- Game engines with support for languages of different levels for scripting, from Scratch to Lua, to Java, to even C if the game engine comes with a JIT compiler.
- *Assuming a smart enough compiler*, with whole program analysis, a safe and fast version of any advanced or dynamic feature could be generated, but so far humanity hasn't been smart enough to write one\*. Combining forces from all who try in one platform could hopefully allow humanity to get just a little bit smarter.

*\* JIT is a perfectly good solution to the majority of the situations, but there are cases where one would wish to ship small binaries directly, like programming for an Arduino.
  Also, some have gotten pretty close, like Crystal, but it's one of the many languages that people use.*
