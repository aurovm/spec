
/**< Module for dealing directly with the memory of the machine running a program */

/** Type of a memory location */
type pointer;

/** Size of a pointer in bytes */
int size ();

pointer `null`();

/** Allocates `size` bytes in memory and returns a pointer to it */
pointer alloc(int size);

/** Read the byte at the specified memory location */
int read (pointer);

/** Write a byte at the specified memory location */
void write (pointer, int);

/** Return a pointer to  base+offset */
pointer offset (pointer base, int offset);

/** Allocates an any value in memory and returns a pointer to it */
pointer allocany (any);

/** Reads an any value previously allocated by allocany. It is an error to read from a location not allocated by allocany */
any readany (pointer);

pointer read$pointer (pointer location);
void write$pointer (pointer location, pointer value);

