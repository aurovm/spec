
private type item = auro.module.item.``;

/** The code object type. */
type ``;

/** Creates an empty code object */
`` `new` (T val, int length);

// Manipulate the signature of the code function
void addinput (``, item);
void addoutput (``, item);

// The code instructions are not structured, the user must ensure the code is
// well formed according to the auro module format

/** Adds a function value to the code instructions **/
void addint (``, item);

/** Adds an integer instruction to the code **/
void addfn (``, item);
