
type buffer;

buffer `new` (int size);
int get (buffer, int index);
void set (buffer, int index, int value);
int size (buffer);
bool readonly (buffer);

// Maybe this method shouldn't be included
void resize (buffer, int size);