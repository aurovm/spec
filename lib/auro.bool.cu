
type bool;
bool `true` ();
bool `false` ();
bool not (bool);
bool and (bool, bool);
bool or (bool, bool);
bool xor (bool, bool);
bool eq (bool, bool);