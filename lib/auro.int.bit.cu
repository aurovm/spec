
// Operations on integers as if they were represented as N bit (N > 15), 2-complement binary.

int not (int);

int and (int, int);
int or (int, int);
int xor (int, int);
int eq (int, int); // also known as xnor

// left shifting overflow is an error
int shl (int, int);
int shr (int, int);

// No wrapping or capping variants of the btishifts are provided, as these types are not appropriate for complicated bit manipulation.