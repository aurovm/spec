
/**< Primitive based on machine bit sizes */

type u8;
u8 new$u8 (int);
int get$u8 (u8);

type u16;
u16 new$u16 (int);
int get$u16 (u16);

type u32;
u32 new$u32 (int);
int get$u32 (u32);

type u64;
u64 new$u64 (int);
int get$u64 (u64);

type i8;
i8 new$i8 (int);
int get$i8 (i8);

type i16;
i16 new$i16 (int);
int get$i16 (i16);

type i32;
i32 new$i32 (int);
int get$i32 (i32);

type i64;
i64 new$i64 (int);
int get$i64 (i64);

type f32;
f32 new$f32 (float);
float get$f32 (f32);

type f64;
f64 new$f64 (float);
float get$f64 (f64);

