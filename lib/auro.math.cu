
float pi ();
float e ();
float sqrt2 ();

float abs (float);
float ceil (float);
float floor (float);
float round (float);
float trunc (float);

float ln (float);
float exp (float);
float sqrt (float);
float cbrt (float);
float pow (float base, float exponent);
float log (float, float base);
float mod (float numerator, float denominator);

float sin (float);
float cos (float);
float tan (float);
float asin (float);
float acos (float);
float atan (float);
float atan2 (float y, float x);
float sinh (float);
float cosh (float);
float tanh (float);
