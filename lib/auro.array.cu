
/** The type contained in the array */
type T = argument.`0`;

/** The array type. */
type ``;

/** Creates an array of the given length, with all indices initialized with val. */
`` new (T val, int length);

/** Creates an array with 0 length, doesn't need an initial value. */
`` empty ();

/** Gets the value at index */
T get (``, int index);

/** Sets the value at index */
void set (``, int index, T val);

/** Computes the number of items in the array. Better called size. */
int len (``);
