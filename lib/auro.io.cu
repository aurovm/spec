
/**< Add hoc utility module for working with system files */

/** File handle */
type file;

/** File pen mode */
type mode;

// Basic file open modes
mode r();
mode w();
mode a();

/** Opens a file in the given mode */
file open (string path, mode);

/** Opens a file in the given mode */
void close (file);

/** Reads a buffer from a file (it should accept a writable buffer instead of creating a new one) */
buffer read (file, int size);

/** Writes a buffer to a file */
void write (file, buffer);

/** Checks whether a file has more data to be read */
bool eof (file);

/** Gets the write/read cursor position of a file */
int pos$get (file);

/** Sets the write/read cursor position of a file */
void pos$set (file, int)

