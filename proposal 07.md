# Auro 0.7

- Remove functor behavior from auro.any and add a build functor for creating any values
- Add an int literal type, which can be examined byte by byte. Int constants would be that type instead of the main int type.

## Context Modules

Add a module kind that when defined, creates two modules: one is a functor, and another is the argument of the functor when it's applied. The argument creates and enters a context that is spread to all items using it, themselves also spreading the context when used by other items. Contexts can be nested but cannot be combined: an item cannot be part of multiple contexts where one does not contain the others.